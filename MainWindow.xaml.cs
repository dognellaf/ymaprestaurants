﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Toolkit.Win32.UI.Controls;
using System.Xaml;
using System.Security.Policy;
using Windows.Networking.Connectivity;
using System.Security.Authentication;
using System.Net.Sockets;
using System.Net;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;
using Microsoft.Toolkit.Wpf.UI.Controls;

namespace YMapRestaurants
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ErrorText.Text = "";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!Internet.CheckConnection())
                ErrorText.Text = "Внимание! Не удалось загрузить карту (нет соединения с интернетом)!";
            else
            {
                web.UpdateLayout();
                web.Navigate(string.Empty);
                web.Navigate("https://ymapdog.000webhostapp.com/");

                ErrorText.Text = "Карта успешно загружена!";
                But.Content = "Обновить карту";
            }
        }
    }

    public static class Internet
    {
        [DllImport("wininet.dll")]
        static extern bool InternetGetConnectedState(ref InternetConnectionState lpdwFlags, int dwReserved);

        [Flags]
        enum InternetConnectionState : int
        {
            INTERNET_CONNECTION_MODEM = 0x1,
            INTERNET_CONNECTION_LAN = 0x2,
            INTERNET_CONNECTION_PROXY = 0x4,
            INTERNET_RAS_INSTALLED = 0x10,
            INTERNET_CONNECTION_OFFLINE = 0x20,
            INTERNET_CONNECTION_CONFIGURED = 0x40
        }

        static object _syncObj = new object();

        /// <summary>
        /// Проверить, есть ли соединение с интернетом
        /// </summary>
        /// <returns></returns>
        public static Boolean CheckConnection()
        {
            lock (_syncObj)
            {
                try
                {
                    InternetConnectionState flags = InternetConnectionState.INTERNET_CONNECTION_CONFIGURED | 0;
                    bool checkStatus = InternetGetConnectedState(ref flags, 0);

                    if (checkStatus)
                        return PingServer(new string[]
                                            {
                                                @"google.com",
                                                @"microsoft.com",
                                                @"ibm.com"
                                            });

                    return checkStatus;
                }
                catch
                {
                    return false;
                }
            }
        }


        /// <summary>
        /// Пингует сервера, при первом получении ответа от любого сервера возвращает true 
        /// </summary>
        /// <param name="serverList">Список серверов</param>
        /// <returns></returns>
        public static bool PingServer(string[] serverList)
        {
            bool haveAnInternetConnection = false;
            Ping ping = new Ping();
            for (int i = 0; i < serverList.Length; i++)
            {
                PingReply pingReply = ping.Send(serverList[i]);
                haveAnInternetConnection = (pingReply.Status == IPStatus.Success);
                if (haveAnInternetConnection)
                    break;
            }

            return haveAnInternetConnection;
        }
    }
}
